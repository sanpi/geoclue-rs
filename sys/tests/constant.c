// Generated by gir (https://github.com/gtk-rs/gir @ 9e7b5ee)
// from gir (@ ???)
// from girs (@ 8e47c67)
// DO NOT EDIT

#include "manual.h"
#include <stdio.h>

#define PRINT_CONSTANT(CONSTANT_NAME) \
    printf("%s;", #CONSTANT_NAME); \
    printf(_Generic((CONSTANT_NAME), \
                    char *: "%s", \
                    const char *: "%s", \
                    char: "%c", \
                    signed char: "%hhd", \
                    unsigned char: "%hhu", \
                    short int: "%hd", \
                    unsigned short int: "%hu", \
                    int: "%d", \
                    unsigned int: "%u", \
                    long: "%ld", \
                    unsigned long: "%lu", \
                    long long: "%lld", \
                    unsigned long long: "%llu", \
                    float: "%f", \
                    double: "%f", \
                    long double: "%ld"), \
           CONSTANT_NAME); \
    printf("\n");

int main() {
    PRINT_CONSTANT((gint) GCLUE_ACCURACY_LEVEL_CITY);
    PRINT_CONSTANT((gint) GCLUE_ACCURACY_LEVEL_COUNTRY);
    PRINT_CONSTANT((gint) GCLUE_ACCURACY_LEVEL_EXACT);
    PRINT_CONSTANT((gint) GCLUE_ACCURACY_LEVEL_NEIGHBORHOOD);
    PRINT_CONSTANT((gint) GCLUE_ACCURACY_LEVEL_NONE);
    PRINT_CONSTANT((gint) GCLUE_ACCURACY_LEVEL_STREET);
    PRINT_CONSTANT((guint) GCLUE_CLIENT_PROXY_CREATE_AUTO_DELETE);
    PRINT_CONSTANT((guint) GCLUE_CLIENT_PROXY_CREATE_NONE);
    return 0;
}
