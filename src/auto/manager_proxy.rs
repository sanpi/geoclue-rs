// This file was generated by gir (https://github.com/gtk-rs/gir)
// from sys/gir
// from sys/girs (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use crate::Manager;
use glib::object::Cast;
use glib::object::IsA;
use glib::translate::*;
use glib::StaticType;
use glib::ToValue;
use std::boxed::Box as Box_;
use std::fmt;
use std::pin::Pin;
use std::ptr;

glib::wrapper! {
    /// The [`ManagerProxy`][crate::ManagerProxy] structure contains only private data and should only be accessed using the provided API.
    ///
    /// # Implements
    ///
    /// [`ManagerExt`][trait@crate::prelude::ManagerExt]
    #[doc(alias = "GClueManagerProxy")]
    pub struct ManagerProxy(Object<ffi::GClueManagerProxy, ffi::GClueManagerProxyClass>) @implements Manager;

    match fn {
        type_ => || ffi::gclue_manager_proxy_get_type(),
    }
}

impl ManagerProxy {
    /// Like [`new_sync()`][Self::new_sync()] but takes a [`gio::BusType`][crate::gio::BusType] instead of a [`gio::DBusConnection`][crate::gio::DBusConnection].
    ///
    /// The calling thread is blocked until a reply is received.
    ///
    /// See [`new_for_bus()`][Self::new_for_bus()] for the asynchronous version of this constructor.
    /// ## `bus_type`
    /// A [`gio::BusType`][crate::gio::BusType].
    /// ## `flags`
    /// Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
    /// ## `name`
    /// A bus name (well-known or unique).
    /// ## `object_path`
    /// An object path.
    /// ## `cancellable`
    /// A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
    ///
    /// # Returns
    ///
    /// The constructed proxy object or [`None`] if `error` is set.
    #[doc(alias = "gclue_manager_proxy_new_for_bus_sync")]
    #[doc(alias = "new_for_bus_sync")]
    pub fn for_bus_sync<P: IsA<gio::Cancellable>>(bus_type: gio::BusType, flags: gio::DBusProxyFlags, name: &str, object_path: &str, cancellable: Option<&P>) -> Result<ManagerProxy, glib::Error> {
        assert_initialized_main_thread!();
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::gclue_manager_proxy_new_for_bus_sync(bus_type.into_glib(), flags.into_glib(), name.to_glib_none().0, object_path.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, &mut error);
            if error.is_null() { Ok(from_glib_full(ret)) } else { Err(from_glib_full(error)) }
        }
    }

    /// Synchronously creates a proxy for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Manager.top_of_page">org.freedesktop.GeoClue2.Manager`</link>`. See `g_dbus_proxy_new_sync()` for more details.
    ///
    /// The calling thread is blocked until a reply is received.
    ///
    /// See [`new()`][Self::new()] for the asynchronous version of this constructor.
    /// ## `connection`
    /// A [`gio::DBusConnection`][crate::gio::DBusConnection].
    /// ## `flags`
    /// Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
    /// ## `name`
    /// A bus name (well-known or unique) or [`None`] if `connection` is not a message bus connection.
    /// ## `object_path`
    /// An object path.
    /// ## `cancellable`
    /// A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
    ///
    /// # Returns
    ///
    /// The constructed proxy object or [`None`] if `error` is set.
    #[doc(alias = "gclue_manager_proxy_new_sync")]
    pub fn new_sync<P: IsA<gio::Cancellable>>(connection: &gio::DBusConnection, flags: gio::DBusProxyFlags, name: Option<&str>, object_path: &str, cancellable: Option<&P>) -> Result<ManagerProxy, glib::Error> {
        assert_initialized_main_thread!();
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::gclue_manager_proxy_new_sync(connection.to_glib_none().0, flags.into_glib(), name.to_glib_none().0, object_path.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, &mut error);
            if error.is_null() { Ok(from_glib_full(ret)) } else { Err(from_glib_full(error)) }
        }
    }

            // rustdoc-stripper-ignore-next
            /// Creates a new builder-pattern struct instance to construct [`ManagerProxy`] objects.
            ///
            /// This method returns an instance of [`ManagerProxyBuilder`] which can be used to create [`ManagerProxy`] objects.
            pub fn builder() -> ManagerProxyBuilder {
                ManagerProxyBuilder::default()
            }
        

    /// Asynchronously creates a proxy for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Manager.top_of_page">org.freedesktop.GeoClue2.Manager`</link>`. See `g_dbus_proxy_new()` for more details.
    ///
    /// When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
    /// You can then call `gclue_manager_proxy_new_finish()` to get the result of the operation.
    ///
    /// See [`new_sync()`][Self::new_sync()] for the synchronous, blocking version of this constructor.
    /// ## `connection`
    /// A [`gio::DBusConnection`][crate::gio::DBusConnection].
    /// ## `flags`
    /// Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
    /// ## `name`
    /// A bus name (well-known or unique) or [`None`] if `connection` is not a message bus connection.
    /// ## `object_path`
    /// An object path.
    /// ## `cancellable`
    /// A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
    /// ## `callback`
    /// A `GAsyncReadyCallback` to call when the request is satisfied.
    #[doc(alias = "gclue_manager_proxy_new")]
    pub fn new<P: IsA<gio::Cancellable>, Q: FnOnce(Result<ManagerProxy, glib::Error>) + Send + 'static>(connection: &gio::DBusConnection, flags: gio::DBusProxyFlags, name: Option<&str>, object_path: &str, cancellable: Option<&P>, callback: Q) {
        assert_initialized_main_thread!();
        let user_data: Box_<Q> = Box_::new(callback);
        unsafe extern "C" fn new_trampoline<Q: FnOnce(Result<ManagerProxy, glib::Error>) + Send + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let ret = ffi::gclue_manager_proxy_new_finish(res, &mut error);
            let result = if error.is_null() { Ok(from_glib_full(ret)) } else { Err(from_glib_full(error)) };
            let callback: Box_<Q> = Box_::from_raw(user_data as *mut _);
            callback(result);
        }
        let callback = new_trampoline::<Q>;
        unsafe {
            ffi::gclue_manager_proxy_new(connection.to_glib_none().0, flags.into_glib(), name.to_glib_none().0, object_path.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }

    
    pub fn new_future(connection: &gio::DBusConnection, flags: gio::DBusProxyFlags, name: Option<&str>, object_path: &str) -> Pin<Box_<dyn std::future::Future<Output = Result<ManagerProxy, glib::Error>> + 'static>> {

        skip_assert_initialized!();
        let connection = connection.clone();
        let name = name.map(ToOwned::to_owned);
        let object_path = String::from(object_path);
        Box_::pin(gio::GioFuture::new(&(), move |_obj, cancellable, send| {
            Self::new(
                &connection,
                flags,
                name.as_ref().map(::std::borrow::Borrow::borrow),
                &object_path,
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }

    /// Like [`new()`][Self::new()] but takes a [`gio::BusType`][crate::gio::BusType] instead of a [`gio::DBusConnection`][crate::gio::DBusConnection].
    ///
    /// When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
    /// You can then call `gclue_manager_proxy_new_for_bus_finish()` to get the result of the operation.
    ///
    /// See [`for_bus_sync()`][Self::for_bus_sync()] for the synchronous, blocking version of this constructor.
    /// ## `bus_type`
    /// A [`gio::BusType`][crate::gio::BusType].
    /// ## `flags`
    /// Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
    /// ## `name`
    /// A bus name (well-known or unique).
    /// ## `object_path`
    /// An object path.
    /// ## `cancellable`
    /// A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
    /// ## `callback`
    /// A `GAsyncReadyCallback` to call when the request is satisfied.
    #[doc(alias = "gclue_manager_proxy_new_for_bus")]
    pub fn new_for_bus<P: IsA<gio::Cancellable>, Q: FnOnce(Result<ManagerProxy, glib::Error>) + Send + 'static>(bus_type: gio::BusType, flags: gio::DBusProxyFlags, name: &str, object_path: &str, cancellable: Option<&P>, callback: Q) {
        assert_initialized_main_thread!();
        let user_data: Box_<Q> = Box_::new(callback);
        unsafe extern "C" fn new_for_bus_trampoline<Q: FnOnce(Result<ManagerProxy, glib::Error>) + Send + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let ret = ffi::gclue_manager_proxy_new_for_bus_finish(res, &mut error);
            let result = if error.is_null() { Ok(from_glib_full(ret)) } else { Err(from_glib_full(error)) };
            let callback: Box_<Q> = Box_::from_raw(user_data as *mut _);
            callback(result);
        }
        let callback = new_for_bus_trampoline::<Q>;
        unsafe {
            ffi::gclue_manager_proxy_new_for_bus(bus_type.into_glib(), flags.into_glib(), name.to_glib_none().0, object_path.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }

    
    pub fn new_for_bus_future(bus_type: gio::BusType, flags: gio::DBusProxyFlags, name: &str, object_path: &str) -> Pin<Box_<dyn std::future::Future<Output = Result<ManagerProxy, glib::Error>> + 'static>> {

        skip_assert_initialized!();
        let name = String::from(name);
        let object_path = String::from(object_path);
        Box_::pin(gio::GioFuture::new(&(), move |_obj, cancellable, send| {
            Self::new_for_bus(
                bus_type,
                flags,
                &name,
                &object_path,
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }
}

#[derive(Clone, Default)]
// rustdoc-stripper-ignore-next
        /// A [builder-pattern] type to construct [`ManagerProxy`] objects.
        ///
        /// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
pub struct ManagerProxyBuilder {
    available_accuracy_level: Option<u32>,
    in_use: Option<bool>,
}

impl ManagerProxyBuilder {
    // rustdoc-stripper-ignore-next
    /// Create a new [`ManagerProxyBuilder`].
    pub fn new() -> Self {
        Self::default()
    }


    // rustdoc-stripper-ignore-next
    /// Build the [`ManagerProxy`].
    pub fn build(self) -> ManagerProxy {
        let mut properties: Vec<(&str, &dyn ToValue)> = vec![];
if let Some(ref available_accuracy_level) = self.available_accuracy_level {
                properties.push(("available-accuracy-level", available_accuracy_level));
            }
if let Some(ref in_use) = self.in_use {
                properties.push(("in-use", in_use));
            }
        glib::Object::new::<ManagerProxy>(&properties)
                .expect("Failed to create an instance of ManagerProxy")

    }

    /// Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.AvailableAccuracyLevel">"AvailableAccuracyLevel"`</link>`.
    ///
    /// Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
    pub fn available_accuracy_level(mut self, available_accuracy_level: u32) -> Self {
        self.available_accuracy_level = Some(available_accuracy_level);
        self
    }

    /// Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.InUse">"InUse"`</link>`.
    ///
    /// Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
    pub fn in_use(mut self, in_use: bool) -> Self {
        self.in_use = Some(in_use);
        self
    }
}

pub const NONE_MANAGER_PROXY: Option<&ManagerProxy> = None;

impl fmt::Display for ManagerProxy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("ManagerProxy")
    }
}
