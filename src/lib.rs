#![doc(html_logo_url = "https://gitlab.com/john_t/geoclue-rs/-/raw/master/logo.png")]

extern crate glib;
extern crate glib_sys;

extern crate libc;
extern crate bitflags;

#[macro_use]
mod macros;

mod auto;
pub use auto::*;
pub mod prelude;
